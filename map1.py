import folium
import pandas

data = pandas.read_csv('Volcanoes.txt')
lat = list(data['LAT'])
lon = list(data['LON'])
elev = list(data['ELEV'])
volcano_name = list(data['NAME'])
volcano_type = list(data['TYPE'])

html = """<h4>%s</h4>
<pre>
Height: %sm 
Type: %s
</pre>
"""

def color_picker(elevation):
    if elevation < 1000:
        return 'green' 
    elif 1000<= elevation < 3000:
        return 'orange'
    else:
        return 'red'

map = folium.Map(location=[38.58, -99.09], zoom_start=6, tiles='Stamen Terrain')

fgv = folium.FeatureGroup(name='Volcanoes')

for lt, ln, el, volcano_name, volcano_type in zip(lat, lon, elev, volcano_name, volcano_type):
    iframe = folium.IFrame(html=html % (volcano_name, el, volcano_type), width=200, height=100)
    fgv.add_child(folium.CircleMarker(location=[lt, ln], radius = 6,  popup=folium.Popup(iframe), fill_color=color_picker(el), color = 'grey', fill_opacity=0.7))

fgp = folium.FeatureGroup(name="Population")
fgp.add_child(folium.GeoJson(data=open('world.json', 'r', encoding='utf-8-sig').read(), style_function=lambda x: {'fillColor':'green' if x['properties']['POP2005'] < 10000000 else 'orange' if 10000000 <= x['properties']['POP2005'] < 35000000 else 'red'}))

map.add_child(fgv)
map.add_child(fgp)
map.add_child(folium.LayerControl())
map.save('Map1.html')